<?php

require_once('modules/module.class.php');
require_once('classes/Tab.class.php');

class ModuleSample extends Module {
  public $pdo;

  public function __construct() {
  }

  public function getName(): string {
    echo "Sample";
  }

  public function getSlug(): string {
    echo "sample";
  }

  public function requiresDBAccess(): bool {
    return true;
  }

  public function setPDOObject(PDO $pdo): void {
    $this->pdo = $pdo;
  }

  public function getTabsInsertedInModuleSettings(): array {
    $tab = new Tab("Sample");
    $tab->setLink('parametrage.php?'.http_build_query(array(
          'module'=>'sample', 'tab'=>5)));
    $tab->setIconClass('fa-life-ring');
    return array($tab);
  }

  public function getModuleSettingsTabContent(int $n_tab): string {
    if ($n_tab == 0) {
      /* Module settings */
      return "<p>Hello world !</p>";
    }
  }
}

